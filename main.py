from pynput.keyboard import Key, Controller
import time, json, os

keyboard = Controller()

# preference file
with open('settings.json') as file:  
    pref = json.load(file)

def sendToChat(message):
    print(f"[OUT] {message}")
    sleepTime = 0.15
    # Minecraft 1.13 somehow breaks the pyput press and release for letter keys. Only special keys work. Here's left Alt for example.
    if pref["minecraft"]["1.13+"]:
        keyboard.press(Key.alt_l)
        time.sleep(sleepTime)
        keyboard.release(Key.alt_l)
    else:
        chatKey = pref["minecraft"]["chatKey"]
        keyboard.press(chatKey)
        time.sleep(sleepTime)
        keyboard.release(chatKey)
    time.sleep(sleepTime)
    keyboard.type(message)
    time.sleep(sleepTime)
    keyboard.press(Key.enter)
    time.sleep(sleepTime)
    keyboard.release(Key.enter)
    time.sleep(sleepTime)

def getLastChatMessage():
    message = None
    with open(f"{pref['mcLocation']}/logs/latest.log") as log:
        # this monster here is kind of slow:
        for line in log:
            parts = line.split(" ")
            try:
                # if this is chat message
                # Minecraft 1.14 has an additional part in logs, "pref["minecraft"]["1.14+"] + " is the fix.
                if parts[pref["minecraft"]["1.14+"] + 2] == "[CHAT]":
                    message = {}
                    # extract time
                    message["time"] = parts[0][1:-1]
                    # player message
                    if parts[pref["minecraft"]["1.14+"] + 3][0] == pref["server"]["msgStartWith"]:
                        message["type"] = "message"
                        message["player"] = parts[3][1:-1]
                        message["content"] = ' '.join(parts[4:])[:-1]
                    else:
                        # join message
                        if parts[pref["minecraft"]["1.14+"] + 4] == pref["server"]["joinMsg"]:
                            message["type"] = "join"
                            message["player"] = parts[3]
                        # leave message
                        elif parts[pref["minecraft"]["1.14+"] + 4] == pref["server"]["leaveMsg"]:
                            message["type"] = "leave"
                            message["player"] = parts[3]
                        # and other situations
                        else:
                            message["type"] = "unknown"
                            message["content"] = ' '.join(parts[3:])[:-1]
            except:
                pass
    return message

def joinRest(arr, start = 1):
    return " ".join(arr[start:])

def multilineToChat(message):
    message = message.replace(" ", "-")
    for line in message.split("\n"):
        sendToChat(line)
        time.sleep(pref["delayBetweenMsgs"])

def processCommands(message):
    print(f"[IN] {message}")
    if message["type"] == "message":
        parts = message["content"].split(" ")
        prefix = pref["bot"]["prefix"]
        isOP = message["player"] == pref["nick"]
        command = parts[pref["minecraft"]["1.14+"] + 0]
        if command == prefix + "test":
            sendToChat("Hello world!")
        elif command == prefix + "scream":
            sendToChat(joinRest(parts).upper())
        elif command == prefix + "sh" and isOP:
            output = os.popen(joinRest(parts)).read()
            multilineToChat(output)
        elif command == prefix + "about":
            multilineToChat("""CraftBot by Grzesiek11
            Bot and pseudo-API for Minecraft chat.
            Source: https://gitlab.com/grzesiek11/minecraft-bot""")
        elif command == prefix + "help":
            multilineToChat("""Prefix: !
            Commands: test, scream, about, help
            OP commands: sh""")

#sendToChat("I just wrote 80 lines of code thanks to VimGod.CC!")

# delay for switching to Minecraft window
time.sleep(pref["startDelay"])
# main loop
oldLastChatMessage = None
while True:
    lastChatMessage = getLastChatMessage()
    if lastChatMessage != oldLastChatMessage:
        processCommands(lastChatMessage)
    oldLastChatMessage = lastChatMessage
